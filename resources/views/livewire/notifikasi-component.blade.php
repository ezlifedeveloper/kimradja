<div class="card">
    <div class="card-header">
        <div class="row">
            <div class="col-6">

            </div>
            <div class="col-6">
                <input type="text" wire:model="searchTerm" placeholder="Search Something..." class="form-control">
            </div>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
                <thead class="text-center">
                    <tr>
                        <th width="5%">No</th>
                        <th class="text-left">Tanggal</th>
                        <th class="text-left">Waktu</th>
                        <th class="text-left">Modul</th>
                        <th class="text-left">Keterangan</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @include('layout.tablecountinfo')
                    @forelse($lists as $list)
                    <tr>
                        <td>{{ 10*($lists->currentPage()-1)+$loop->iteration}}</td>
                        <td class="text-left">{{ $list->created_at->format('d M Y') }}</td>
                        <td class="text-left">{{ $list->created_at->format('H:i:s') }}</td>
                        <td class="text-left">{{ $list->modul }}</td>
                        <td class="text-left">{{ $list->title }}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5">No Data Available</td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
        </div>
        @if($lists->hasPages())
            {{ $lists->links() }}
        @endif
    </div>
</div>
