<li class="nav-item dropdown">
    <a class="nav-link" data-toggle="dropdown" href="#">
        <i class="far fa-bell"></i>
        @if($unseen)<span class="badge badge-warning navbar-badge">{{$unseen}}</span>@endif
    </a>
    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        @forelse ($lists as $item)
        <div class="dropdown-divider"></div>
        <a wire:click="openNotif({{$item}})" class="dropdown-item" style="background: rgb(236, 234, 234)">
            <div class="row">
                <div class="col-1">
                    <i class="fas fa-{{$item->icon}} mr-1"></i>
                </div>
                <p class="col-11" style="word-wrap: break-word">{{$item->title}}</p>
            </div>
            <div class="row">
                <div class="col-1"></div>
                <span class="col-5 text-muted text-xs">{{$item->modul}}</span>
                <span class="col-6 text-muted text-xs text-right">{{$item->created_at->diffForHumans()}}</span>
            </div>
        </a>
        @empty
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item text-center">
            Tidak ada notifikasi
        </a>
        @endforelse
        @if($unseen)
        <div class="dropdown-divider"></div>
        <a wire:click="readAll()" class="dropdown-item dropdown-footer">Tandai Semua Sebagai Dibaca</a>
        @endif
        <div class="dropdown-divider"></div>
        <a href="{{route('notifikasi')}}" class="dropdown-item dropdown-footer">Lihat Semua</a>
    </div>
</li>
