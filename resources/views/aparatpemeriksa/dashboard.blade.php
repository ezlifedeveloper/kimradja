@extends('app', ['title' => 'Dashboard Tinjut'])
@include('layout.function')
@section('content')

<div class="container-fluid">
    <div class="row">
        <div class="col-lg-8">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Rekapitulasi Aparat Pemeriksa Per Tahun</h3>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="barChart" style="height: 255px; min-height: 230px; display: block; width: 100%;" width="1504" height="460" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-lg-4">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Rekapitulasi Per Jenis Pengawasan {{date('Y')}}</h3>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="prosesChart" style="height: 255px;"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Rekapitulasi Per Status Aksi UIC {{date('Y')}}</h3>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="statusuicChart" style="height: 275px;"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Rekapitulasi Per Status Forum APK {{date('Y')}}</h3>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="statusapkChart" style="height: 275px;"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Rekapitulasi Per Status Forum BPK {{date('Y')}}</h3>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="statusbpkChart" style="height: 275px;"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Rekapitulasi Per Status Approval {{date('Y')}}</h3>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="statusapprovalChart" style="height: 275px;"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>

</div>

@section('js')
<!-- fullCalendar 2.2.5 -->
<script src="{{ asset('admin/plugins/chart.js/Chart.min.js') }}"></script>

<!-- Page specific script -->
@if(Auth::user()->role == 'superuser')
<script>
    var statusapprovalData = {
        labels: @php echo json_encode($data['statapproval']['label']); @endphp,
        datasets: [{
            data: @php echo json_encode($data['statapproval']['data']); @endphp,
            backgroundColor : @php echo json_encode($data['statapproval']['color']); @endphp,
        }]
    };

    var statusapprovalChartCanvas = $('#statusapprovalChart').get(0).getContext('2d')
    var statusapprovalOptions     = {
        maintainAspectRatio : false,
        responsive : true,
    }

    var statusapprovalChart = new Chart(statusapprovalChartCanvas, {
        type: 'pie',
        data: statusapprovalData,
        options: statusapprovalOptions
    })
</script>
<script>
    var statusuicData = {
        labels: @php echo json_encode($data['statuic']['label']); @endphp,
        datasets: [{
            data: @php echo json_encode($data['statuic']['data']); @endphp,
            backgroundColor : @php echo json_encode($data['statuic']['color']); @endphp,
        }]
    };

    var statusuicChartCanvas = $('#statusuicChart').get(0).getContext('2d')
    var statusuicOptions     = {
        maintainAspectRatio : false,
        responsive : true,
    }

    var statusuicChart = new Chart(statusuicChartCanvas, {
        type: 'pie',
        data: statusuicData,
        options: statusuicOptions
    })
</script>
<script>
    var statusapkData = {
        labels: @php echo json_encode($data['statapk']['label']); @endphp,
        datasets: [{
            data: @php echo json_encode($data['statapk']['data']); @endphp,
            backgroundColor : @php echo json_encode($data['statapk']['color']); @endphp,
        }]
    };

    var statusapkChartCanvas = $('#statusapkChart').get(0).getContext('2d')
    var statusapkOptions     = {
        maintainAspectRatio : false,
        responsive : true,
    }

    var statusapkChart = new Chart(statusapkChartCanvas, {
        type: 'pie',
        data: statusapkData,
        options: statusapkOptions
    })
</script>
<script>
    var statusbpkData = {
        labels: @php echo json_encode($data['statbpk']['label']); @endphp,
        datasets: [{
            data: @php echo json_encode($data['statbpk']['data']); @endphp,
            backgroundColor : @php echo json_encode($data['statbpk']['color']); @endphp,
        }]
    };

    var statusbpkChartCanvas = $('#statusbpkChart').get(0).getContext('2d')
    var statusbpkOptions     = {
        maintainAspectRatio : false,
        responsive : true,
    }

    var statusbpkChart = new Chart(statusbpkChartCanvas, {
        type: 'pie',
        data: statusbpkData,
        options: statusbpkOptions
    })
</script>

<script>
    var prosesData = {
        labels: @php echo json_encode($data['jp']['label']); @endphp,
        datasets: [{
            data: @php echo json_encode($data['jp']['data']); @endphp,
            backgroundColor : @php echo json_encode($data['jp']['color']); @endphp,
        }]
    };

    var prosesChartCanvas = $('#prosesChart').get(0).getContext('2d')
    var prosesOptions     = {
        maintainAspectRatio : false,
        responsive : true,
        legend: {
            position: "right",
            align: "middle"
        },
    }

    var prosesChart = new Chart(prosesChartCanvas, {
        type: 'pie',
        data: prosesData,
        options: prosesOptions
    })
</script>

<script>
    var areaChartData = {
        labels  : @php echo json_encode($data['barlabel']); @endphp,
        datasets: @php echo json_encode($data['bar']); @endphp
    }

    var barChartCanvas = $('#barChart').get(0).getContext('2d')
    var barChartData = jQuery.extend(true, {}, areaChartData)

    var barChartOptions = {
        responsive              : true,
        maintainAspectRatio     : false,
        datasetFill             : true,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }

    var barChart = new Chart(barChartCanvas, {
        type: 'bar',
        data: barChartData,
        options: barChartOptions
    })
</script>
@endif
@endsection

@endsection
