@extends('app', ['title' => 'Dashboard Advokasi'])

@section('content')

@section('css')
<link rel="stylesheet" href="{{ asset('admin/plugins/fullcalendar/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/fullcalendar-daygrid/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/fullcalendar-timegrid/main.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/plugins/fullcalendar-bootstrap/main.min.css') }}">
@endsection

<div class="container-fluid">
    <div class="row">
        @if(Auth::user()->role == 'superuser')
        <div class="col-lg-6">
            <div class="card card-primary">
                <div class="card-body p-0">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        <div class="col-lg-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Rekapitulasi Sub Modul Per Tahun</h3>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="barChart" style="height: 255px; min-height: 230px; display: block; width: 100%;" width="1504" height="460" class="chartjs-render-monitor"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Rekapitulasi Per Jenis Perkara {{date('Y')}}</h3>
                </div>
                <div class="card-body">
                    <div class="chart">
                        <canvas id="prosesChart" style="height: 255px;"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        @else
        <div class="col-lg-12">
            <div class="card card-primary">
                <div class="card-body p-0">
                    <!-- THE CALENDAR -->
                    <div id="calendar"></div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
        @endif
    </div>

</div>

@section('js')
<!-- fullCalendar 2.2.5 -->
<script src="{{ asset('admin/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('admin/plugins/fullcalendar/main.min.js') }}"></script>
<script src="{{ asset('admin/plugins/fullcalendar-daygrid/main.min.js') }}"></script>
<script src="{{ asset('admin/plugins/fullcalendar-timegrid/main.min.js') }}"></script>
<script src="{{ asset('admin/plugins/fullcalendar-interaction/main.min.js') }}"></script>
<script src="{{ asset('admin/plugins/fullcalendar-bootstrap/main.min.js') }}"></script>
<script src="{{ asset('admin/plugins/chart.js/Chart.min.js') }}"></script>

<!-- Page specific script -->
<script>
    $(function () {
        var date = new Date()
        var d    = date.getDate(),
            m    = date.getMonth(),
            y    = date.getFullYear()

        var Calendar = FullCalendar.Calendar;

        var calendarEl = document.getElementById('calendar');

        // initialize the external events
        // -----------------------------------------------------------------

        var calendar = new Calendar(calendarEl, {
            plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid' ],
            header    : {
                left  : 'prev,next today',
                center: 'title',
                right : 'dayGridMonth,timeGridWeek,timeGridDay'
            },
            'themeSystem': 'bootstrap',
            //Random default events
            events    : @php echo json_encode($data['event']); @endphp
        });

        calendar.render();
        $('#calendar').fullCalendar()
    })
</script>

@if(Auth::user()->role == 'superuser')
    @if($data['jp']['label'])
    <script>
        var prosesData = {
            labels: @php echo json_encode($data['jp']['label']); @endphp,
            datasets: [{
                data: @php echo json_encode($data['jp']['data']); @endphp,
                backgroundColor : @php echo json_encode($data['jp']['color']); @endphp,
            }]
        };

        var prosesChartCanvas = $('#prosesChart').get(0).getContext('2d')
        var prosesOptions     = {
            maintainAspectRatio : false,
            responsive : true,
            legend: {
                position: "right",
                align: "middle"
            },
        }

        var prosesChart = new Chart(prosesChartCanvas, {
            type: 'pie',
            data: prosesData,
            options: prosesOptions
        })
    </script>
    @endif

    @if($data['barlabel'])
    <script>
        var areaChartData = {
            labels  : @php echo json_encode($data['barlabel']); @endphp,
            datasets: [{
                    label               : 'Penanganan Perkara',
                    backgroundColor     : 'rgba(60,141,188,0.9)',
                    data                : @php echo json_encode($data['bar']['data'][0]); @endphp
                }, {
                    label               : 'Pendampingan',
                    backgroundColor     : 'rgba(210, 214, 222, 1)',
                    data                : @php echo json_encode($data['bar']['data'][1]); @endphp
                }, {
                    label               : 'Pendapat Hukum',
                    backgroundColor     : 'rgba(245, 159, 193, 1)',
                    data                : @php echo json_encode($data['bar']['data'][2]); @endphp
                },
            ]
        }

        var barChartCanvas = $('#barChart').get(0).getContext('2d')
        var barChartData = jQuery.extend(true, {}, areaChartData)

        var barChartOptions = {
            responsive              : true,
            maintainAspectRatio     : false,
            datasetFill             : true,
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }

        var barChart = new Chart(barChartCanvas, {
            type: 'bar',
            data: barChartData,
            options: barChartOptions
        })
    </script>
    @endif
@endif
@endsection

@endsection
