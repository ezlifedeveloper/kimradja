<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>{{'Layanan Aduan'.' | '.env('APP_NAME')}}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('admin/plugins/fontawesome-free/css/all.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css') }}">
        <!-- icheck bootstrap -->
        <link rel="stylesheet" href="{{ asset('admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
        <!-- Tempusdominus Bbootstrap 4 -->
        <link rel="stylesheet" href="{{ asset('admin/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('admin/css/adminlte.min.css') }}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

        <style>
            body{
                background: url(gdjuanda.jpeg) no-repeat center center fixed;
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;
            }
        </style>
    </head>
    <body class="hold-transition text-md">
        <div class="wrapper">
            <div class="content">
                <div class="container">
                    <div class="row d-flex justify-content-center">
                        <div class="col-lg-8">
                            <div class="card" style="margin-top: 50px">
                                <div class="card-header bg-primary">
                                    <h5 class="text-center">Sampaikan Laporan Anda</h5>
                                </div>
                                <div class="card-body">
                                    <a class="d-flex justify-content-center" href="{{route('login')}}" style="color: blue; margin-bottom:20px">Sudah punya akun? Klik untuk login</a>
                                    <span class="contact100-form">
                                        @if(session('alert-success'))
                                            <div class="container-fluid">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class='alert alert-success alert-dismissible' id="alert">
                                                            <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                                            <h4><i class='icon fa fa-check'></i> {{session('alert-success')}}</h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @elseif(session('alert'))
                                        <div class="container-fluid">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class='alert alert-danger alert-dismissible' id="alert">
                                                        <button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button>
                                                        <h4><i class='icon fa fa-times'></i> {{session('alert')}}</h4>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @endif
                                    </span>
                                    <form action="{{route('aduan')}}" id="formaduan" method="POST" enctype="multipart/form-data">
                                        @csrf
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Nama Anda</label>
                                            <div class="col-md-9 col-8">
                                                <input class="form-control" type="text" name="name" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Email Anda</label>
                                            <div class="col-md-9 col-8">
                                                <input class="form-control" type="email" name="email" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">No. HP Anda</label>
                                            <div class="col-md-9 col-8">
                                                <input class="form-control" type="phone" name="phone" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">NIK / NIP</label>
                                            <div class="col-md-9 col-8">
                                                <input class="form-control" type="text" name="nik" required>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Jenis Laporan</label>
                                            <div class="col-md-9 col-8">
                                                <select class="form-control" name="type" required>
                                                    @foreach($data['jenis'] as $d)
                                                    <option value="{{$d->id}}">{{$d->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Perihal Laporan</label>
                                            <div class="col-md-9 col-8">
                                                <input class="form-control" type="text" name="perihal" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Tanggal Kejadian</label>
                                            <div class="col-md-9 col-8">
                                                <input type="date" name="tanggal" class="form-control" data-inputmask-alias="date"
                                                    data-inputmask-inputformat="dd/mm/yyyy">
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Lokasi Kejadian</label>
                                            <div class="col-md-9 col-8">
                                                <input class="form-control" type="text" name="lokasi" required>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Kronologi Kejadian</label>
                                            <div class="col-md-9 col-8">
                                                <textarea class="form-control" name="kronologi" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Motif Kejadian</label>
                                            <div class="col-md-9 col-8">
                                                <textarea class="form-control" name="motif" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Pihak yang Terlibat</label>
                                            <div class="col-md-9 col-8">
                                                <textarea class="form-control" name="pihak" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Hubungan dengan Terlapor</label>
                                            <div class="col-md-9 col-8">
                                                <textarea class="form-control" name="relasi" rows="3" required></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <label class="col-md-3 col-4 col-form-label">Lampiran</label>
                                            <div class="col-md-9 col-8">
                                                <input type="file" name="file[]" multiple>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="card-footer text-right">
                                    <button class="btn btn-primary" form="formaduan" type="submit">Submit Laporan</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="{{ asset('admin/plugins/jquery/jquery.min.js') }}"></script>
        <!-- Bootstrap 4 -->
        <script src="{{ asset('admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset('admin/dist/js/adminlte.min.js') }}"></script>

    </body>
</html>
