<?php

namespace App\Http\Livewire\Aparatpemeriksa;

use App\Exports\TinjutExport;
use App\Helper\FormatHelper;
use App\Imports\TinjutImport;
use App\Models\AparatPemeriksa;
use App\Models\DataTinjut;
use App\Models\FileTinjut;
use App\Models\JenisPemeriksaan;
use App\Models\JenisPengawasan;
use App\Models\StatusAksi;
use App\Models\TemuanTinjut;
use App\Models\ReferensiUnit;
use App\Models\UnitTinjutEs2;
use App\Models\UnitTinjutEs3;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithFileUploads;
use Livewire\WithPagination;
use Maatwebsite\Excel\Facades\Excel;
use Jantinnerezo\LivewireAlert\LivewireAlert;
use stdClass;

class TinjutComponent extends Component
{
    // Load addon trait
    use WithPagination, WithFileUploads, LivewireAlert;

    // Bootsrap pagination
    protected $paginationTheme = 'bootstrap';

    // Public variable
    public $isOpen = 0;
    public $isTimeline = 0;
    public $isEs2 = 0;
    public $isEs3 = 0;
    public $isImport = 0;
    public $isExport = 0;
    public $paginatedPerPages = 10;
    public $input_id, $searchTerm, $input_tahun, $input_uic_es3, $input_uic_es2, $input_uic_es1, $input_nomor_temuan, $input_judul;
    public $input_jenis_pemeriksaan, $input_kode_rekomendasi, $input_uraian_rekomendasi, $input_uraian_rencana, $input_target;
    public $input_aparat_pemeriksa, $input_aparat_pemeriksa_lainnya;
    public $input_status_uic, $input_status_apk, $input_forum_bpk, $input_status_bpk, $input_approval;
    public $input_tinjut, $input_keterangan, $input_catatan;
    public $photos = [];
    public $filterEs2, $filterStatus, $filterJenis, $filterAparat;
    public $file;

    // View
    public function render()
    {
        $searchData = $this->searchTerm;
        $user = Auth::user();
        $userid = $user->id;
        $userunit = $user->es2;

        $filteres2 = $this->filterEs2 ?? 'all';
        $filterstatus = $this->filterStatus ?? 'all';
        $filterjenis = $this->filterJenis ?? 'all';
        $filteraparat = $this->filterAparat ?? 'all';

        return view('livewire.aparatpemeriksa.tinjut-component', [
            // Lists
            'loggedUser' => $user,
            'temuanTinjut' => TemuanTinjut::where('id', $this->input_id)->first(),
            'listData' => DataTinjut::where('tinjut', $this->input_id)->get(),
            'listTypes' => JenisPengawasan::orderBy('name')->get(),
            'listActionTypes' => StatusAksi::all(),
            'listAparat' => AparatPemeriksa::all(),
            'listEs1' => ReferensiUnit::select('es1')->distinct()->get(),
            'listUnit' => ReferensiUnit::select('es2')->distinct()->get(),
            'lists' => TemuanTinjut::when($user->role != 'superuser', function ($searchById) use ($userunit) {
                $searchById->whereHas('es2', function ($searchById) use ($userunit) {
                    $searchById->where('unit', $userunit);
                });
            })->when($searchData, function ($searchQuery) use ($searchData) {
                $searchQuery->where(function ($searchQuery) use ($searchData) {
                    $searchQuery->where('tahun', 'like', '%' . $searchData . '%')
                        ->orWhere('uraian_rekomendasi', 'like', '%' . $searchData . '%')
                        ->orWhere('tinjut', 'like', '%' . $searchData . '%')
                        ->orWhere('judul', 'like', '%' . $searchData . '%')
                        ->orWhere('kode_rekomendasi', 'like', '%' . $searchData . '%')
                        ->orWhereHas('es2', function ($searchById) use ($searchData) {
                            $searchById->where('unit', $searchData);
                        })->orWhereHas('es3', function ($searchById) use ($searchData) {
                            $searchById->where('unit', $searchData);
                        });
                });
            })->when($filteres2 != 'all', function ($searchById) use ($filteres2) {
                $searchById->whereHas('es2', function ($searchById) use ($filteres2) {
                    $searchById->where('unit', $filteres2);
                });
            })->when($filterstatus != 'all', function ($searchById) use ($filterstatus) {
                $searchById->where(function ($searchById) use ($filterstatus) {
                    $searchById->where('status_uic', $filterstatus)
                        ->orWhere('status_apk', $filterstatus)
                        ->orWhere('forum_bpk', $filterstatus);
                });
            })->when($filterjenis != 'all', function ($searchById) use ($filterjenis) {
                $searchById->where('jenis_pemeriksaan', $filterjenis);
            })->when($filteraparat != 'all', function ($searchById) use ($filteraparat) {
                $searchById->where('aparat_pemeriksa', $filteraparat);
            })->orderBy('approval', 'asc')->orderBy('tahun', 'desc')->orderBy('target', 'asc')->paginate($this->paginatedPerPages),
        ]);
    }

    public function importExcel()
    {
        $this->validate([
            'file' => 'required|mimes:csv,xls,xlsx'
        ]);

        Excel::import(new TinjutImport(Auth::user()), $this->file);

        $this->alert('success', 'Import data berhasil');

        $this->closeModal();
        $this->resetInputFields();
    }

    public function exportExcel()
    {
        $tinjut = TemuanTinjut::orderBy('tahun', 'desc')->orderBy('target', 'desc')->get();

        $data = [];
        $i = 0;
        foreach ($tinjut as $tt) {
            $dd = new stdClass;
            $dd->id = ++$i;
            $dd->tahun = $tt->tahun;
            $dd->nomor_temuan = $tt->nomor_temuan;
            $dd->kode_rekomendasi = $tt->kode_rekomendasi;
            $dd->jenis_pemeriksaan = $tt->type->name;
            $dd->aparat_pemeriksa = $tt->pemeriksa->name;
            $dd->aparat_pemeriksa_lainnya = $tt->aparat_pemeriksa_lainnya;
            $dd->uic_es1 = $tt->uic_es1;

            $es2 = [];
            $es3 = [];
            foreach ($tt->es2 as $e) $es2[] = $e->unit;
            foreach ($tt->es3 as $e) $es3[] = $e->unit;

            $dd->uic_es2 = implode(", ", $es2);
            $dd->uic_es3 = implode(", ", $es3);

            $dd->judul = $tt->judul;
            $dd->target = FormatHelper::formatDate($tt->target);
            $dd->uraian_rekomendasi = $tt->uraian_rekomendasi;
            $dd->keterangan = $tt->keterangan;
            $dd->catatan = $tt->catatan;
            $dd->tinjut = $tt->tinjut;
            $dd->status_uic = $tt->statusUic->name;
            $dd->status_apk = $tt->statusApk->name;
            $dd->forum_bpk = $tt->forumBpk->name;
            $dd->approval = $tt->approval ? 'Approved' : 'Pending';
            $dd->created_at = FormatHelper::formatTimestamp($tt->created_at);
            $dd->created_by = $tt->creator->name;
            $dd->updated_at = FormatHelper::formatTimestamp($tt->updated_at);
            $dd->updated_by = $tt->updater->name;

            $data[] = $dd;
        }

        return Excel::download(new TinjutExport($data), time() . '_TINJUT.xlsx');
    }

    // Reset input fields
    private function resetInputFields()
    {
        $this->reset([
            'input_id', 'input_tahun', 'input_uic_es3', 'input_uic_es2', 'input_uic_es1', 'input_nomor_temuan', 'input_judul',
            'input_jenis_pemeriksaan', 'input_kode_rekomendasi', 'input_uraian_rekomendasi', 'input_target', 'input_aparat_pemeriksa',
            'input_status_uic', 'input_status_apk', 'input_forum_bpk', 'input_status_bpk', 'input_approval', 'input_aparat_pemeriksa_lainnya',
            'file'
        ]);
    }

    public function openImport()
    {
        $this->isOpen = false;
        $this->isTimeline = false;
        $this->isEs2 = false;
        $this->isEs3 = false;
        $this->isImport = true;
        $this->isExport = false;
    }

    // Open input form
    public function openModal()
    {
        $this->isOpen = true;
        $this->isTimeline = false;
        $this->isEs2 = false;
        $this->isEs3 = false;
        $this->isImport = false;
        $this->isExport = false;
    }

    // Open input form
    public function openTimeline($id)
    {
        $this->input_id = $id;
        $this->isTimeline = true;
        $this->isOpen = false;
        $this->isEs2 = false;
        $this->isEs3 = false;
        $this->isImport = false;
        $this->isExport = false;
    }

    // Open input form
    public function openEs2($id)
    {
        $this->input_id = $id;
        $this->isTimeline = false;
        $this->isOpen = false;
        $this->isEs2 = true;
        $this->isEs3 = false;
        $this->isImport = false;
        $this->isExport = false;
    }

    // Open input form
    public function openEs3($id)
    {
        $this->input_id = $id;
        $this->isTimeline = false;
        $this->isOpen = false;
        $this->isEs2 = false;
        $this->isEs3 = true;
        $this->isImport = false;
        $this->isExport = false;
    }

    // Close input form
    public function closeModal()
    {
        $this->isOpen = false;
        $this->isTimeline = false;
        $this->isEs2 = false;
        $this->isEs3 = false;
        $this->isImport = false;
        $this->isExport = false;
        $this->resetInputFields();
    }

    // Open input form and then reset input fields
    public function create()
    {
        $this->openModal();
        $this->resetInputFields();
    }

    // Save data
    public function storeEs2()
    {
        $messages = [
            '*.required' => 'This column is required',
            '*.numeric' => 'This column is required to be filled in with number',
            '*.string' => 'This column is required to be filled in with letters',
        ];

        $this->validate([
            'input_uic_es2' => 'required|string',
        ], $messages);

        $data = UnitTinjutEs2::create([
            'tinjut' => $this->input_id,
            'unit' => $this->input_uic_es2,
            'created_by' => Auth::id(),
        ]);

        $unitUser = User::where('role', 'admines2')->where('es2', $this->input_uic_es2)->first();
        $temuan = TemuanTinjut::where('id', $this->input_id)->first();
        if ($unitUser) FormatHelper::createNotification($unitUser->id, 'Monitoring Tinjut', 'Ada temuan ' . $temuan->nomor_temuan . ' yang harus Anda respons', true);

        // Show an alert
        $this->alert('success', 'Data berhasil disimpan');

        // Close input form, we're going back to the list
        $this->closeModal();

        // Reset input fields for next input
        $this->resetInputFields();
    }

    // Save data
    public function storeEs3()
    {
        $messages = [
            '*.required' => 'This column is required',
            '*.numeric' => 'This column is required to be filled in with number',
            '*.string' => 'This column is required to be filled in with letters',
        ];

        $this->validate([
            'input_uic_es3' => 'required|string',
        ], $messages);

        $data = UnitTinjutEs3::create([
            'tinjut' => $this->input_id,
            'unit' => $this->input_uic_es3,
            'created_by' => Auth::id(),
        ]);

        // Show an alert
        $this->alert('success', 'Data berhasil disimpan');

        // Close input form, we're going back to the list
        $this->closeModal();

        // Reset input fields for next input
        $this->resetInputFields();
    }

    // Save data
    public function store()
    {
        $messages = [
            '*.required' => 'This column is required',
            '*.numeric' => 'This column is required to be filled in with number',
            '*.string' => 'This column is required to be filled in with letters',
        ];

        $this->validate([
            'input_tahun' => 'required|size:4',
            'input_uic_es1' => 'required|string',
            'input_nomor_temuan' => 'required|string',
            'input_judul' => 'required|string',
            'input_kode_rekomendasi' => 'required|string',
            'input_jenis_pemeriksaan' => 'required',
            'input_aparat_pemeriksa' => 'required',
            'input_status_uic' => 'required',
            'input_status_apk' => 'required',
            'input_forum_bpk' => 'required',
            'input_approval' => 'required',
        ], $messages);

        $user = Auth::user();

        if (!in_array($user->role, ['admin', 'superuser'])) {
            $data = DataTinjut::create([
                'tinjut' => $this->input_id,
                'uraian' => $this->input_tinjut,
                'keterangan' => $this->input_keterangan,
                'catatan' => $this->input_catatan,
                'created_by' => Auth::id()
            ]);

            $update = TemuanTinjut::where('id', $this->input_id)
                ->update([
                    'updated_by' => Auth::id(),
                    'tinjut' => $this->input_tinjut,
                    'keterangan' => $this->input_keterangan,
                    'catatan' => $this->input_catatan,
                ]);

            $id = $data->id;

            foreach ($this->photos as $photo) {
                $fileName = time() . '_' . $id . '_' . strtolower(preg_replace('/\s+/', '_', $photo->getClientOriginalName()));
                $photo->storeAs('tinjut', $fileName);

                FileTinjut::insert([
                    'tinjut' => $this->input_id,
                    'data' => $data->id,
                    'name' => $fileName,
                    'created_by' => Auth::id(),
                    'file' => env('APP_URL') . '/storage/tinjut/' . $fileName,
                ]);
            }

            $temuan = TemuanTinjut::where('id', $id)->first();
            FormatHelper::createNotification(Auth::id(), 'Monitoring Tinjut', $user->name . ' melakukan update data temuan ' . $temuan->nomor_temuan, true);
        } else {
            $data = TemuanTinjut::updateOrCreate(['id' => $this->input_id], [
                'tahun' => $this->input_tahun,
                'uic_es1' => $this->input_uic_es1,
                'nomor_temuan' => $this->input_nomor_temuan,
                'judul' => $this->input_judul,
                'kode_rekomendasi' => $this->input_kode_rekomendasi,
                'jenis_pemeriksaan' => $this->input_jenis_pemeriksaan,
                'uraian_rekomendasi' => $this->input_uraian_rekomendasi,
                'aparat_pemeriksa' => $this->input_aparat_pemeriksa,
                'aparat_pemeriksa_lainnya' => $this->input_aparat_pemeriksa_lainnya,
                'target' => $this->input_target,
                'status_uic' => $this->input_status_uic,
                'status_apk' => $this->input_status_apk,
                'forum_bpk' => $this->input_forum_bpk,
                'approval' => $this->input_approval,
                'created_by' => Auth::id(),
                'updated_by' => Auth::id(),
            ]);
        }

        // Show an alert
        $this->alert('success', $this->input_id ? 'Data berhasil diperbarui' : 'Data berhasil disimpan');

        $this->closeModal();
        $this->resetInputFields();
    }

    // Approve data
    public function approve($id, $status)
    {
        TemuanTinjut::where('id', $id)->update(['status' => 1]);

        // Show an alert
        $this->alert('success', 'Data berhasil diapprove');
    }

    // Parse data to input form
    public function edit($id)
    {
        // Find data from the $id
        $data = TemuanTinjut::findOrFail($id);

        // Parse data from the $data variable
        $this->input_id = $id;
        $this->input_tahun = $data->tahun;
        $this->input_uic_es3 = $data->uic_es3;
        $this->input_uic_es2 = $data->uic_es2;
        $this->input_uic_es1 = $data->uic_es1;
        $this->input_nomor_temuan = $data->nomor_temuan;
        $this->input_judul = $data->judul;
        $this->input_kode_rekomendasi = $data->kode_rekomendasi;
        $this->input_jenis_pemeriksaan = $data->jenis_pemeriksaan;
        $this->input_uraian_rekomendasi = $data->uraian_rekomendasi;
        $this->input_aparat_pemeriksa = $data->aparat_pemeriksa;
        $this->input_aparat_pemeriksa_lainnya = $data->aparat_pemeriksa_lainnya;
        $this->input_target = $data->target;
        $this->input_status_uic = $data->status_uic;
        $this->input_status_apk = $data->status_apk;
        $this->input_forum_bpk = $data->forum_bpk;
        $this->input_approval = $data->approval;
        $this->input_tinjut = $data->tinjut;
        $this->input_keterangan = $data->keterangan;
        $this->input_catatan = $data->catatan;

        // Then input fields and show data
        $this->openModal();
    }

    // Delete data
    public function deleteEs2($id)
    {
        // Find existing photo
        $sql = UnitTinjutEs2::where('id', $id)->firstOrFail();

        // Delete Data from DB
        $sql->find($id)->delete();

        // Show an alert
        $this->alert('warning', 'Data berhasil dihapus');
    }

    // Delete data
    public function deleteEs3($id)
    {
        // Find existing photo
        $sql = UnitTinjutEs3::where('id', $id)->firstOrFail();

        // Delete Data from DB
        $sql->find($id)->delete();

        // Show an alert
        $this->alert('warning', 'Data berhasil dihapus');
    }

    // Delete data
    public function delete($id)
    {
        // Find existing photo
        $sql = TemuanTinjut::where('id', $id)->firstOrFail();

        // Delete Data from DB
        $sql->find($id)->delete();

        // Show an alert
        $this->alert('warning', 'Data berhasil dihapus');
    }

    public function deleteData($id)
    {
        // Find existing photo
        $sql = DataTinjut::where('id', $id)->firstOrFail();

        // Delete Data from DB
        $sql->find($id)->delete();

        // Show an alert
        $this->alert('warning', 'Data berhasil dihapus');
    }

    public function deleteFile($id)
    {
        // Find existing photo
        $sql = FileTinjut::select('file')->where('id', $id)->firstOrFail();

        // Delete Data from DB
        $sql->find($id)->delete();

        // Then delete it
        unlink(storage_path('app/tinjut/' . substr(str_replace(env('APP_URL') . '/storage/tinjut', "", $sql->file), 1)));

        // Show an alert
        $this->alert('warning', 'Data berhasil dihapus');
    }
}
