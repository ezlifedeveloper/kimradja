<?php

namespace App\Http\Livewire;

use App\Models\Notifikasi;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;
use Livewire\WithPagination;

class NotifikasiComponent extends Component
{
    use WithPagination;
    public $paginatedPerPages = 20;
    public $searchTerm;

    public function render()
    {
        $searchData = $this->searchTerm;

        $user = Auth::user();
        $role = $user->role;
        $userid = $user->id;

        return view('livewire.notifikasi-component', [
            'lists' => Notifikasi::when($role != 'superuser', function ($searchQuery) use ($userid) {
                $searchQuery->where('user', $userid);
            })->when($role == 'superuser', function ($searchQuery) {
                $searchQuery->where('admin', true);
            })->when($searchData, function ($searchQuery) use ($searchData) {
                return $searchQuery->where(function ($searchQuery) use ($searchData) {
                    $searchQuery->where('modul', 'like', '%' . $searchData . '%')
                        ->orWhere('title', 'like', '%' . $searchData . '%');
                });
            })->orderBy('created_at', 'desc')->paginate($this->paginatedPerPages)
        ]);
    }
}
