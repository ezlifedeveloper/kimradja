<?php

namespace App\Http\Livewire;

use App\Models\Notifikasi;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class NotificationIconComponent extends Component
{
    public function render()
    {
        $user = Auth::user();
        $role = $user->role;
        $userid = $user->id;

        $data = Notifikasi::when($role != 'superuser', function ($searchQuery) use ($userid) {
            $searchQuery->where('user', $userid)->where('read', false);
        })->when($role == 'superuser', function ($searchQuery) {
            $searchQuery->where('admin', true)->where('admin_read', false);
        })->orderBy('created_at', 'desc')->get();

        return view('livewire.notification-icon-component', [
            'lists' => $data,
            'unseen' => $data->count()
        ]);
    }

    public function readAll()
    {
        $user = Auth::user();
        $role = $user->role;
        $userid = $user->id;
        if ($role == 'superuser') Notifikasi::where('admin', true)->update(['admin_read' => true]);
        else Notifikasi::where('user', $userid)->update(['read' => true]);
    }

    public function openNotif(Notifikasi $item)
    {
        $modul = $item->modul;

        $user = Auth::user();
        $role = $user->role;

        if ($role == 'superuser') {
            Notifikasi::where('id', $item->id)->update(['admin_read' => true]);
        } else {
            Notifikasi::where('id', $item->id)->update(['read' => true]);
        }

        if ($modul == 'Penanganan Perkara') return redirect()->route('penangananperkara');
        else if ($modul == 'Pendampingan') return redirect()->route('pendampingan');
        else if ($modul == 'Pendapat Hukum') return redirect()->route('pendapathukum');
        else if ($modul == 'Pengaduan') return redirect()->route('pengaduan');
        else if ($modul == 'Monitoring Tinjut') return redirect()->route('tinjut');
        else if ($modul == 'Pemeriksaan') return redirect()->route('pengawasan');
        else if ($modul == 'EPITE') return redirect()->route('epite');
        else if ($modul == 'PIPK') return redirect()->route('ppita');
        else if ($modul == 'PPITA') return redirect()->route('pipk');
        else if ($modul == 'Piagam Risiko') return redirect()->route('piagamrisiko');
        else if ($modul == 'Profil Risiko') return redirect()->route('profilrisiko');
        else if ($modul == 'Mitigasi Risiko') return redirect()->route('mitigasirisiko');
    }
}
