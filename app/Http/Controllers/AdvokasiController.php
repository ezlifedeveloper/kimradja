<?php

namespace App\Http\Controllers;

use App\Helper\FormatHelper;
use App\Models\JenisPerkara;
use App\Models\PendapatHukum;
use App\Models\PerkaraAdvokasi;
use App\Models\SidangPerkara;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use stdClass;

class AdvokasiController extends Controller
{
    //
    public function kalender(Request $request)
    {
        $user = Auth::user();
        $role = $user->role;
        $userid = $user->id;

        $data['jenisdata'] = [];
        $data['jenis'] = [];
        $data['jenislabel'] = [];
        $data['jeniscolor'] = [];
        $data['barlabel'] = [];
        $data['bar']['data'] = [];

        $bardata = [];

        $dataLitigasi = SidangPerkara::whereHas('parent', function ($query) use ($role, $userid) {
            return $query->when($role == 'user', function ($searchQuery) use ($userid) {
                return $searchQuery->where('created_by', $userid);
            });
        })->get();
        $dataNonLitigasi = PendapatHukum::when($role == 'user', function ($searchQuery) use ($userid) {
            return $searchQuery->where('created_by', $userid);
        })->get();

        $datal = PerkaraAdvokasi::without(['jenisPerkara', 'file'])->selectRaw('tahun_masuk as tahun, modul, type, count(id) as count')
            ->groupBy('tahun', 'modul', 'type')
            ->orderBy('tahun')
            ->get();
        $datan = PendapatHukum::without(['jenisPerkara', 'file'])->selectRaw('SUBSTRING(tanggal, 1, 4) as tahun, type, count(id) as count')
            ->groupBy('tahun', 'type')
            ->orderBy('tahun')
            ->get();

        $datajp = JenisPerkara::select('id', 'name')->get();
        foreach ($datajp as $jp) {
            $data['jp']['label'][$jp->id - 1] = $jp->name;
            $data['jp']['data'][$jp->id - 1] = 0;
            $data['jp']['color'][$jp->id - 1] = FormatHelper::getRandomColor();
        }

        foreach ($datal as $d) {
            if ($data['barlabel'] == null) $data['barlabel'][] = $d->tahun;
            else if (!in_array($d->tahun, $data['barlabel'])) $data['barlabel'][] = $d->tahun;

            for ($i = 0; $i < 3; $i++) if (!isset($bardata[$i][$d->tahun])) $bardata[$i][$d->tahun] = 0;

            $bardata[$d->modul - 1][$d->tahun] = ($bardata[$d->modul - 1][$d->tahun] ?? 0) + $d->count;
            if ($d->tahun == date('Y')) $data['jp']['data'][$d->type - 1] += $d->count;
        }

        foreach ($datan as $d) {
            $bardata[2][$d->tahun] = ($bardata[2][$d->tahun] ?? 0) + $d->count;
            if ($d->tahun == date('Y')) $data['jp']['data'][$d->type - 1] += $d->count;
        }

        foreach ($bardata as $key => $d) {
            $i = 0;
            foreach ($data['barlabel'] as $dd) {
                $data['bar']['data'][$key][$i] = array_key_exists($dd, $d) ? $d[$dd] : 0;
                $i++;
            }
        }

        $data['event'] = [];

        foreach ($dataLitigasi as $d) {
            $dataTemp = new stdClass;
            $dataTemp->title = 'Sidang ' . $d->jenisSidang->name . ' ' . $d->nomor_st;
            $dataTemp->start = $d->tanggal;
            if ($d->modul == 1) {
                $dataTemp->backgroundColor = '#0073b7';
                $dataTemp->borderColor = '#0073b7';
            } else {
                $dataTemp->backgroundColor = '#f39c12';
                $dataTemp->borderColor = '#f39c12';
            }
            $dataTemp->allDay = true;

            $data['event'][] = $dataTemp;
        }

        foreach ($dataNonLitigasi as $d) {
            $dataTemp = new stdClass;
            $dataTemp->title = 'Permintaan Bantuan Hukum ' . $d->unit;
            $dataTemp->start = $d->tanggal;
            $dataTemp->backgroundColor = '#00c0ef';
            $dataTemp->borderColor = '#00c0ef';
            $dataTemp->allDay = true;

            $data['event'][] = $dataTemp;
        }

        return view('advokasi.kalenderkegiatan')->with('data', $data);
    }
}
