<?php

namespace App\Http\Controllers;

use App\Helper\FormatHelper;
use App\Models\AparatPemeriksa;
use App\Models\DataTinjut;
use App\Models\JenisPemeriksaan;
use App\Models\JenisPengawasan;
use App\Models\StatusAksi;
use App\Models\TemuanTinjut;
use Illuminate\Http\Request;
use stdClass;

class TinjutController extends Controller
{
    //
    public function dashboard(Request $request)
    {
        $query = TemuanTinjut::without(['statusUIC', 'statusAPK', 'forumBPK'])
            ->selectRaw('tahun, jenis_pemeriksaan, aparat_pemeriksa, status_uic, status_apk, forum_bpk, approval, count(id) as count')
            ->groupBy('tahun', 'jenis_pemeriksaan', 'aparat_pemeriksa', 'status_uic', 'status_apk', 'forum_bpk', 'approval')
            ->orderBy('tahun', 'asc')
            ->get();

        $datajp = JenisPengawasan::select('id', 'name')->get();
        foreach ($datajp as $jp) {
            $data['jp']['label'][$jp->id - 1] = $jp->name;
            $data['jp']['data'][$jp->id - 1] = 0;
            $data['jp']['color'][$jp->id - 1] = FormatHelper::getRandomColor();
        }

        $bar = [];
        $datal = AparatPemeriksa::select('id', 'name')->get();
        foreach ($datal as $d) {
            $label[$d->id - 1] = $d->name;
            $bar[$d->id - 1][0] = 0;
        }

        $data['statapproval']['label'][0] = 'Pending';
        $data['statapproval']['data'][0] = 0;
        $data['statapproval']['color'][0] = FormatHelper::getRandomColor();
        $data['statapproval']['label'][1] = 'Approved';
        $data['statapproval']['data'][1] = 0;
        $data['statapproval']['color'][1] = FormatHelper::getRandomColor();

        $datastat = StatusAksi::select('id', 'name')->get();
        foreach ($datastat as $stat) {
            $data['statapk']['label'][$stat->id - 1] = $stat->name;
            $data['statapk']['data'][$stat->id - 1] = 0;
            $data['statapk']['color'][$stat->id - 1] = FormatHelper::getRandomColor();
            $data['statuic']['label'][$stat->id - 1] = $stat->name;
            $data['statuic']['data'][$stat->id - 1] = 0;
            $data['statuic']['color'][$stat->id - 1] = FormatHelper::getRandomColor();
            $data['statbpk']['label'][$stat->id - 1] = $stat->name;
            $data['statbpk']['data'][$stat->id - 1] = 0;
            $data['statbpk']['color'][$stat->id - 1] = FormatHelper::getRandomColor();
        }

        $data['barlabel'] = [];
        $bardata = [];

        foreach ($query as $d) {
            if ($data['barlabel'] == null) $data['barlabel'][] = $d->tahun;
            else if (!in_array($d->tahun, $data['barlabel'])) $data['barlabel'][] = $d->tahun;

            $bardata[$d->aparat_pemeriksa - 1][$d->tahun] = ($bardata[$d->aparat_pemeriksa - 1][$d->tahun] ?? 0) + $d->count;
            if ($d->tahun == date('Y')) $data['jp']['data'][$d->jenis_pemeriksaan - 1] += $d->count;
            if ($d->tahun == date('Y')) $data['statbpk']['data'][$d->forum_bpk - 1] += $d->count;
            if ($d->tahun == date('Y')) $data['statapk']['data'][$d->status_apk - 1] += $d->count;
            if ($d->tahun == date('Y')) $data['statuic']['data'][$d->status_uic - 1] += $d->count;
            if ($d->tahun == date('Y')) $data['statapproval']['data'][$d->approval] += $d->count;
        }

        foreach ($bardata as $key => $d) {
            $i = 0;
            foreach ($data['barlabel'] as $dd) {
                $bar[$key][$i] = array_key_exists($dd, $d) ? $d[$dd] : 0;
                $i++;
            }
        }

        $data['bar'] = [];
        foreach ($bar as $key => $d) {
            $ap = new stdClass;
            $ap->label = $label[$key];
            $ap->backgroundColor = FormatHelper::getRandomColor();
            $ap->data = $d;
            $data['bar'][] = $ap;
        }

        return view('aparatpemeriksa.dashboard')->with('data', $data);
    }
}
