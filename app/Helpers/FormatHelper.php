<?php

namespace App\Helper;

use App\Models\Notification;
use App\Models\Notifikasi;
use App\Models\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

class FormatHelper
{
    public static function formatDate($date)
    {
        if (!$date) return null;
        $date = date_create($date);
        return date_format($date, "d M Y");
    }

    public static function formatTimestamp($timestamp)
    {
        $date = substr($timestamp, 0, 10);
        $time = substr($timestamp, 12);
        $date = date_create($date);
        return date_format($date, "d M Y") . ' ' . $time;
    }

    public static function createNotification($user, $modul, $title, $admin)
    {
        if ($modul == 'Penanganan Perkara') $icon = 'gavel';
        else if ($modul == 'Pendampingan') $icon = 'users';
        else if ($modul == 'Pendapat Hukum') $icon = 'comment';
        else if ($modul == 'Pengaduan') $icon = 'flag';
        else if ($modul == 'Monitoring Tinjut') $icon = 'desktop';
        else if ($modul == 'Pemeriksaan') $icon = 'search';
        else if (in_array($modul, ['EPITE', 'PIPK', 'PPITA'])) $icon = 'cogs';
        else if ($modul == 'Piagam Risiko') $icon = 'address-card';
        else if ($modul == 'Profil Risiko') $icon = 'power-off';
        else if ($modul == 'Mitigasi Risiko') $icon = 'database';

        $notifikasi = Notifikasi::create([
            'user' => $user,
            'modul' => $modul,
            'title' => $title,
            'icon' => $icon,
            'admin' => $admin
        ]);
    }

    public static function getRandomColor()
    {
        return '#' . str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT) .
            str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT) .
            str_pad(dechex(mt_rand(0, 255)), 2, '0', STR_PAD_LEFT);;
    }
}
