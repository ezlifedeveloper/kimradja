<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PIExport implements FromArray, WithHeadings, WithMapping
{
    protected $data;

    function __construct($data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            'No',
            'Tahun',
            'Kegiatan',
            'Auditee',
            'Perencanaan',
            'Pelaksanaan',
            'Pelaporan',
            'Created At',
            'Created By',
        ];
    }

    public function map($data): array
    {
        return [
            $data->id,
            $data->tahun,
            $data->kegiatan,
            $data->auditee,
            $data->perencanaan,
            $data->pelaksanaan,
            $data->pelaporan,
            $data->created_at,
            $data->created_by,
        ];
    }
}
