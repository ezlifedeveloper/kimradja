<?php

namespace App\Exports;

use App\Models\TemuanTinjut;
use Maatwebsite\Excel\Concerns\FromArray;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class TinjutExport implements FromArray, WithHeadings, WithMapping
{
    protected $data;

    function __construct($data)
    {
        $this->data = $data;
    }

    public function array(): array
    {
        return $this->data;
    }

    public function headings(): array
    {
        return [
            'ID',
            'Tahun',
            'Nomor Temuan',
            'Kode Rekomendasi',
            'Jenis Pemeriksaan',
            'Aparat Pemeriksa',
            'Aparat Pemeriksa Lainnya',
            'UIC ES 1',
            'UIC ES 2',
            'UIC ES 3',
            'Judul',
            'Target',
            'Uraian Rekomendasi',
            'Keterangan',
            'Catatan',
            'Tinjut',
            'Status UIC',
            'Status APK',
            'Status Forum BPK',
            'Approval',
            'Created At',
            'Created By',
            'Last Updated At',
            'Last Updated By',
        ];
    }

    public function map($data): array
    {
        return [
            $data->id,
            $data->tahun,
            $data->nomor_temuan,
            $data->kode_rekomendasi,
            $data->jenis_pemeriksaan,
            $data->aparat_pemeriksa,
            $data->aparat_pemeriksa_lainnya,
            $data->uic_es1,
            $data->uic_es2,
            $data->uic_es3,
            $data->judul,
            $data->target,
            $data->uraian_rekomendasi,
            $data->keterangan,
            $data->catatan,
            $data->tinjut,
            $data->status_uic,
            $data->status_apk,
            $data->forum_bpk,
            $data->approval,
            $data->created_at,
            $data->created_by,
            $data->updated_at,
            $data->updated_by,
        ];
    }
}
