<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UnitTinjutEs2 extends Model
{
    use HasFactory;

    protected $table = 'unit_tinjut_es2';
    protected $guarded = [];
}
