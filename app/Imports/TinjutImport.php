<?php

namespace App\Imports;

use App\Models\TemuanTinjut;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithStartRow;

class TinjutImport implements ToModel, WithStartRow
{
    protected $user;

    function __construct($user)
    {
        $this->user = $user;
    }

    public function model(array $row)
    {
        if ($row[0] == null) return;
        return new TemuanTinjut([
            'tahun' => $row[0],
            'uic_es1' => $row[1],
            'nomor_temuan' => $row[2],
            'judul' => $row[3],
            'kode_rekomendasi' => $row[4],
            'jenis_pemeriksaan' => $row[5],
            'uraian_rekomendasi' => $row[6],
            'aparat_pemeriksa' => $row[7],
            'aparat_pemeriksa_lainnya' => $row[8],
            'target' => $row[9],
            'status_uic' => $row[10],
            'status_apk' => $row[11],
            'forum_bpk' => $row[12],
            'approval' => $row[13],
            'created_by' => $this->user->id,
            'updated_by' => $this->user->id,
            'created_at' => Carbon::now()->toDateTimeString(),
            'updated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }

    public function startRow(): int
    {
        return 2;
    }
}
