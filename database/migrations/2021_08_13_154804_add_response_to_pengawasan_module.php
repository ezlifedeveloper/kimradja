<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddResponseToPengawasanModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('request_pengawasan_units', function (Blueprint $table) {
            $table->string('response')->nullable();
        });
        Schema::table('undangan_pengawasans', function (Blueprint $table) {
            $table->string('response')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('request_pengawasan_units', function (Blueprint $table) {
            $table->dropColumn('response');
        });
        Schema::table('undangan_pengawasans', function (Blueprint $table) {
            $table->dropColumn('response');
        });
    }
}
