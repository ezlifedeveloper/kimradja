<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUnitTinjutEs3sTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('temuan_tinjuts', function (Blueprint $table) {
            $table->dropColumn('uic_es3');
        });

        Schema::create('unit_tinjut_es3', function (Blueprint $table) {
            $table->id();
            $table->foreignId('tinjut')->constraint('temuan_tinjuts');
            $table->string('unit');
            $table->foreignId('created_by')->constraint('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('unit_tinjut_es3');

        Schema::table('temuan_tinjuts', function (Blueprint $table) {
            $table->string('uic_es3');
        });
    }
}
